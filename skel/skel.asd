; -*- lisp -*-

(defpackage #:skel.system
  (:use #:common-lisp #:asdf #:cl-librarian))
(in-package #:skel.system)

;; Use shelf of external libraries
;; (shelf.lisp needs to be loaded before loading this system)
(use-shelf (find-shelf :skel))

(defsystem #:skel
  :name "Skeleton Project"
  :version "0"

  :depends-on (#:hunchentoot #:postmodern)
  :components
  ((:module #:src
            :components ((:file "package")))))
