;;; -*- lisp -*- run.lisp: start loaded project.

(postmodern:connect-toplevel "" "" "" "localhost")
(skel:init)
(hunchentoot:start-server :port 4242)

(setf swank:*use-dedicated-output-stream* nil
      swank:*communication-style* :fd-handler)
(swank:create-server :port 2342
                     :external-format :utf-8-unix
                     :dont-close t)

(gc :full t)
