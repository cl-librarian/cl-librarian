;;; -*- lisp -*- shelf.lisp: set up library pathnames and load shelf definition.
;;; see README for notes on how to set up before running.
(require 'asdf)

(defpackage #:skel.shelf
  (:use #:common-lisp))
(in-package #:skel.shelf)

(defvar *topdir*
  (make-pathname :defaults (or *load-pathname*
                               *default-pathname-defaults*)
                 :name nil :type nil :version nil))

(defun subdir (subdir &optional (base-dir *topdir*))
  (merge-pathnames (make-pathname :directory `(:relative ,subdir))
                   base-dir))

(unless (find-package :cl-librarian)
  (pushnew (subdir "cl-librarian") asdf:*central-registry* :test #'equal)
  (asdf:operate 'asdf:load-op :cl-librarian))
(use-package :cl-librarian)

(defshelf skel (postmodern hunchentoot) ; e.g.
  ((slime tarball-repo :source "http://common-lisp.net/project/slime/slime-2.0.tgz"))
  :directory (subdir "libraries"))

