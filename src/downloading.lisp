;;;; -*- lisp -*-
;;;; Copyright (c) 2007-2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.
;;;; See COPYING file for details.
(in-package #:cl-librarian)

;;; Downloading single files
(defun get-file (address directory &aux (colon (position #\: address)))
  (if colon
      (get-file-using-protocol
       (intern (string-upcase (subseq address 0 colon)) :keyword)
       (subseq address (1+ colon))
       directory)
      (get-file-using-protocol :file address directory)))

(defgeneric get-file-using-protocol (protocol address directory)
  (:documentation "Download file by protocol `PROTOCOL' from address `ADDRESS' into `DIRECTORY'.  Return full pathname of downloaded file."))

;;; FIXME: use explicit full path
(defmethod get-file-using-protocol
    (protocol address directory
     &aux
     (err (with-output-to-string (e)
	    (external-program:run *wget-command* `("-nv"
						   "-P" ,(namestring directory)
						   "--no-cookies"
						   "--header" ,(format nil "Cookie: CCLAN-SITE=~A" *cclan-mirror*)
						   ,(concatenate 'string (string-downcase (string protocol)) ":" address))
				  :output e))))
  "Default file getter: use wget."
  (print err)
  (pathname (subseq err (1+ (position #\" err)) (position #\" err :from-end t))))

(defmethod get-file-using-protocol ((protocol (eql :asdf-install)) address directory)
  "Pseudo-protocol :ASDF-INSTALL to fetch ASDF-installable files."
  (get-file-using-protocol :http (concatenate 'string
                                              "//www.cliki.net/" address "?download")
                           directory))

(defmethod get-file-using-protocol ((protocol (eql :cclan)) address directory)
  "Pseudo-protocol :CCLAN to get files from CClan"
  (get-file (concatenate 'string *cclan-mirror* address) directory))

(defmethod get-file-using-protocol ((protocol (eql :file)) address directory
                                    &aux (target (merge-pathnames directory address)))
  (unless (string= (namestring address) (namestring target))
    (copy-file address target))
  target)

(defmethod get-file-using-protocol ((protocol (eql :scp)) address directory)
  (external-program:run *scp-command* (list address (namestring directory)))
  (merge-pathnames (subseq address (1+ (or (position #\/ address)
                                           (position #\: address))))
                   directory))
