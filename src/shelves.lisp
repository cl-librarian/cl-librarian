;;;; -*- lisp -*-
;;;; Copyright (c) 2007-2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.
;;;; See COPYING file for details.
(in-package #:cl-librarian)

(defclass shelf ()
  ((name :initarg :name :accessor shelf-name)
   (directory :initform nil :initarg :directory :accessor shelf-directory)
   (includes :initarg :includes :accessor shelf-includes)
   (included-by :initform nil :accessor shelf-included-by)
   (immediate-contents :initarg :immediate-contents :accessor shelf-immediate-contents)
   (contents :accessor shelf-contents)
   (root-systems :accessor shelf-root-systems :initarg :root-systems :initform nil)))

(defmethod shared-initialize :after ((shelf shelf) slots &key &allow-other-keys)
  (declare (ignore slots))
  (unless (shelf-directory shelf)
    (setf (shelf-directory shelf)
          (merge-pathnames (make-pathname :directory `(:relative ,(string-downcase (string (shelf-name shelf)))))
                           (make-pathname :defaults (or *load-pathname* *default-pathname-defaults*)
                                          :name nil :type nil :version nil)))))

(defun shelf-subdir (shelf subdir)
  (merge-pathnames
   (make-pathname :directory `(:relative ,subdir))
   (shelf-directory shelf)))

(defpackage :cl-librarian.shelves (:use))
(defvar *shelves* (make-hash-table))

(defun canonicalize-shelf-name (name)
  (intern (string name) :cl-librarian.shelves))

(defun finalize-shelf (shelf
                       &aux (includes (mapcar #'find-shelf
                                              (shelf-includes shelf))))
  "Finalize shelf library list."

  ;; Calculate actual contents
  (setf (shelf-contents shelf)
        (remove-duplicates
         (apply #'append (shelf-immediate-contents shelf)
                (mapcar #'shelf-contents includes))))

  ;; Notify included shelves
  (dolist (include includes)
    (pushnew shelf (shelf-included-by include)))

  ;; Update shelves that depend on us
  (map nil #'finalize-shelf (shelf-included-by shelf))

  shelf)

(defun define-shelf (name &key (includes nil includes-p) (immediate-contents nil immediate-contents-p) directory root-systems)
  "DEFSHELF's workhorse."
  (setf name (canonicalize-shelf-name name))

  (let ((shelf (or (gethash name *shelves*) ; When shelf exists, we only update it
                   (setf (gethash name *shelves*) ; Otherwise, we must create one
                         (make-instance 'shelf :name name :directory directory :root-systems root-systems)))))
    (when includes-p
      (setf (shelf-includes shelf) includes))
    (when immediate-contents-p
      (setf (shelf-immediate-contents shelf) immediate-contents))
    (finalize-shelf shelf)))

(defmacro defshelf (name includes immediate-contents &key directory root-systems)
  `(define-shelf ',name
       :includes ',includes
       :immediate-contents (list
                            ,@(mapcar #'(lambda (lib)
                                          (if (atom lib)
                                              `(find-library ',lib)
                                              `(deflibrary ,@lib)))
                                      immediate-contents))
       ,@(when directory `(:directory ,directory))
       ,@(when root-systems `(:root-systems ',root-systems))))

(defun find-shelf (name &key force-reload)
  "Find shelf `NAME', returning its shelf object.

`NAME' can be a symbol or string.  If it's a symbol or string not
containing the colon and dot characters, it names the shelf that is
already managed.  If the shelf isn't already loaded, or `FORCE-RELOAD'
is true, an appropriate .shelf file from `*SHELVES-DIR*' directory is
loaded, and the shelf object is returned.

If `NAME' is a pathname or any other string, it is treated as an URL
or full pathname to a .shelf file.  If `NAME' is an existing file name
or pathname, the file is LOADed; if `NAME' is an URL, it is downloaded
into `*SHELVES-DIR*' and then loaded.

Returns NIL if shelf is not found."

  (cond ((symbolp name)                 ; plain name as a symbol
         (or
          ;; Try to retrieve from memory
          (unless force-reload
            (gethash (canonicalize-shelf-name name) *shelves*))

          ;; Load from file
          (let ((shelf-path (make-pathname :defaults *shelves-dir*
                                           :type "shelf"
                                           :name (string-downcase (string name)))))
            (when (probe-file shelf-path)
              (load shelf-path :verbose t)
              (find-shelf name)))))

        ;; plain name as a string
        ((and (stringp name)
              (not (or (position #\: name)
                       (position #\. name))))
         (find-shelf (intern (string-upcase name) :cl-librarian.shelves)))

        ;; shelf file pathname or address
        (t
         (let ((full-pathname (if (probe-file name)
                                  (pathname name)
                                  (get-file name *shelves-dir*))))
           (load full-pathname :verbose t)
           (find-shelf (intern (string-upcase (pathname-name full-pathname))
                               :cl-librarian.shelves))))))

(defmacro with-shelf ((var &optional (shelf var)) &body body)
  "Set VAR to shelf object specified by SHELF.

Shelf object specified by SHELF can be either a shelf, a symbol
specifying shelf name, or a string with shelf name or address.  If
SHELF is not given, it defaults to current value of VAR.

If SHELF is not found by name or address, or if it's neither shelf,
symbol nor string, an error is signalled."
  `(let ((,var (etypecase ,shelf
                 ((or symbol string pathname)
                  (or (find-shelf ,shelf)
                      (error "No shelf named ~A" ,shelf)))
                 (shelf ,shelf))))
     ,@body))

(defun download-shelf (shelf)
  "Download libraries included in `SHELF' into shelf's directory.

`SHELF' may be a SHELF instance, or a symbol or string.  If it's
a symbol or string, `FIND-SHELF' is invoked to get the SHELF
instance."
  (with-shelf (shelf)
    (multiple-value-bind (shelf-dir created)
        (ensure-directories-exist (shelf-directory shelf))
      (declare (ignore shelf-dir))
      (let ((site (shelf-subdir shelf "site")))
        (ensure-directories-exist site)
        (format t "; ~:[Completing~;Downloading~] shelf ~A to directory ~A...~%" created (shelf-name shelf) site)
        (dolist (repo (shelf-contents shelf))
          (let ((repo-dir (merge-pathnames (repo-name-as-string repo) site)))
            (if (probe-file repo-dir)
                (format t "; Repo ~A already downloaded.~%" repo)
                (progn (format t "; Downloading ~A...~%" repo)
                       (vc-get repo site)
                       (when (patch repo)
                         (format t ";  Patching...~%")
			 (external-program:run *patch-command*
					       `("-p0"
						 "-d" ,(namestring repo-dir))
					       :input (patch repo)))
                       #+FIXME!shell-command-in-dir
		       (when (after-download-hook repo)
                         (format t ";  Running ~A in ~A...~%"
                                 (after-download-hook repo) repo-dir)
                         (shell-command-in-dir repo-dir (after-download-hook repo)))))))
        (format t "; Downloaded whole shelf.~%"))
      (values))))

(defvar *used-shelves* ()
  "List of shelves currently in use.")

(defun use-shelf (shelf &key (download-if-necessary t))
  "Add SHELF (shelf object, name or shelf file pathname) systems to ASDF central registry.

Returns shelf object for SHELF."
  (with-shelf (shelf)
    (when (and download-if-necessary
               (not (probe-file (shelf-directory shelf)))
               (not (probe-file (shelf-subdir shelf "site"))))
      (download-shelf shelf))
    (pushnew shelf *used-shelves*)
    shelf))

(defun unuse-shelf (shelf)
  "Remove SHELF systems from ASDF central registry."
  (with-shelf (shelf)
    (setf *used-shelves*
          (delete shelf *used-shelves*))))

(defun asdf-operate-on-shelf (shelf operation &rest args)
  "Perform ASDF operation OPERATION on shelf SHELF"
  (with-shelf (shelf)
    (mapcar #'(lambda (system)
                (apply #'asdf:operate operation system args))
            (shelf-root-systems shelf))))

(defun load-shelf (shelf)
  "Load shelf's root ASDF systems."
   (restart-case (asdf-operate-on-shelf shelf 'asdf:load-op)
     (download-shelf ()
       :report (lambda (s) (format s "Download shelf ~A." shelf))
       :test (lambda (c) (typep c 'asdf:missing-dependency))
       (download-shelf shelf)
       (asdf-operate-on-shelf shelf 'asdf:load-op))))

(defun shelf&repo-systems (shelf repo)
  (mapcan #'(lambda (pathname)
              (directory
               (merge-pathnames pathname
                                (merge-directory-pathname (repo-name-as-string repo)
                                                          (shelf-subdir shelf "site")))))
          (system-pathnames repo)))

(defun search-system-in-shelves (system &aux (system-name (asdf::coerce-name system)))
  "Search for ASDF system SYSTEM in used shelves.

This function is added to ASDF:*SYSTEM-DEFINITION-SEARCH-FUNCTIONS*."
  (some #'(lambda (shelf)
              (some #'(lambda (repo)
                        (some #'(lambda (pathname)
                                  (and (string= (pathname-name pathname)
                                                system-name)
                                       pathname))
                              (shelf&repo-systems shelf repo)))
                    (shelf-contents shelf)))
          *used-shelves*))

;; EVAL-WHEN anyone?
(pushnew 'search-system-in-shelves asdf:*system-definition-search-functions*)
