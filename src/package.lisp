;;;; -*- lisp -*-
;;;; Copyright (c) 2007-2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.
;;;; See COPYING file for details.
(defpackage #:cl-librarian
  (:use #:common-lisp #:split-sequence)
  (:export #:shelf #:defshelf #:find-shelf #:with-shelf
           #:download-shelf
           #:get-file #:get-file-using-protocol
           #:find-library #:deflibrary #:undeflibrary
           #:repo #:darcs-repo #:svn-repo #:cvs-repo #:git-repo #:hg-repo #:tarball-repo
           #:system-pathnames
           #:use-shelf #:unuse-shelf #:*used-shelves* #:search-system-in-shelves
           #:asdf-operate-on-shelf #:load-shelf
           #:*asdf-install-fallback* #:*shelves-dir* #:*git-command* #:*hg-command*
           #:*svn-command* #:*cvs-command* #:*darcs-command*
           #:*wget-command* #:*scp-command* #:*tar-command*
           #:+cclan-mirrors+ #:*cclan-mirror*))
