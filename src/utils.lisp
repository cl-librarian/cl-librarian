;;;; -*- lisp -*-
;;;; Copyright (c) 2007-2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.
;;;; See COPYING file for details.
(in-package #:cl-librarian)

(defun merge-directory-pathname (subdir &optional (basedir *default-pathname-defaults*))
  "Add subdir to pathname."
  (when (symbolp subdir)
    (setf subdir (string-downcase (string subdir))))
  (make-pathname :defaults basedir :directory (append (pathname-directory basedir)
                                                      (list (string subdir)))))

(defun copy-file (src dst &aux (buf (make-array 4096 :element-type '(unsigned-byte 8))))
  (with-open-file (src-stream src :direction :input :element-type '(unsigned-byte 8))
    (with-open-file (dst-stream dst :direction :output :element-type '(unsigned-byte 8))
      (loop for pos = (read-sequence buf src-stream)
         while (plusp pos)
         do (write-sequence buf dst-stream :end pos)))))
