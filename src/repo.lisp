;;;; -*- lisp -*-
;;;; Copyright (c) 2007-2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.
;;;; See COPYING file for details.
(in-package #:cl-librarian)

(defvar *libraries* ()
  "List of all defined libraries.")

(defpackage :cl-librarian.libraries (:use))
(defun canonicalize-library-name (name)
  (intern (string-upcase (string name)) :cl-librarian.libraries))

;;; Base repo class and protocol
(defclass repo ()
  ((type :allocation :class :initform "Dummy" :initarg :%type :reader %type)
   (name :accessor name)
   (after-download-hook :initarg :after-download-hook :accessor after-download-hook :initform nil)
   (patch :initarg :patch :accessor patch :initform nil)
   (system-pathnames :initarg :system-pathnames :accessor system-pathnames
                     :initform (list (make-pathname :name :wild :type "asd"))))
  (:documentation "A source of a single library."))

(defmethod shared-initialize
    :after ((repo repo) slots &key name &allow-other-keys)
  (declare (ignore slots))
  (setf (name repo) (canonicalize-library-name name)))

(defgeneric source (repo)
  (:documentation "Return single string describing repo source.")
  (:method ((repo repo)) "the middle of Nowhere"))

(defmethod print-object ((o repo) s)
  (print-unreadable-object (o s :type nil :identity t)
    (format s "~A repo ~A at ~A"
            (%type o) (name o) (source o))))

(defgeneric same-class-repo-equalp (repo1 repo2)
  (:documentation "Return true if two libraries are equal.")
  (:method (repo1 repo2)
    (error "Not implemented."))
  (:method  ((repo1 repo) (repo2 repo))
    (and (eq (name repo1) (name repo2))
         (string= (source repo1) (source repo2)))))

(defun repo-equalp (repo1 repo2)
  (assert (subtypep (class-of repo1) (find-class 'repo))
          (repo1) "~S is not a repo" repo1)
  (assert (subtypep (class-of repo2) (find-class 'repo))
          (repo2) "~S is not a repo" repo2)
  (and (eq (class-of repo1) (class-of repo2))
       (same-class-repo-equalp repo1 repo2)))

(defgeneric vc-get (repo directory)
  (:documentation "Download repo REPO into subdirectory of DIRECTORY named (NAME REPO).
Return resulting directory.")
  (:method ((repo t) directory)
    (error "Downloading abstract base repo class is not possible.")))

(defun repo-name-as-string (repo)
  (string-downcase (symbol-name (name repo))))

;;; Sample concrete repo class (those will finally go into separate file)

(defclass darcs-repo (repo)
  ((type :allocation :class :initform "Darcs" :initarg :%type :reader %type)
   (source :initarg :source :reader source))
  (:documentation "Repo managed by darcs version control system."))

(defmethod vc-get ((repo darcs-repo) directory
		   &aux (target-dir (merge-directory-pathname (repo-name-as-string repo) directory)))
  (external-program:run *darcs-command*
			`("get"
			  "--partial" "-q"
			  ,(source repo)
			  ,(namestring target-dir)))
  target-dir)


(defclass cvs-repo (repo)
  ((type :allocation :class :initform "CVS" :initarg :%type :reader %type)
   (repository :initarg :repository :reader repository)
   (module :initarg :module :reader module))
  (:documentation "Repo managed by CVS version control system."))

(defmethod source ((repo cvs-repo))
  (format nil "~A,~A" (repository repo) (module repo)))

(defmethod vc-get ((repo cvs-repo) directory
		   &aux (target-dir (merge-directory-pathname (repo-name-as-string repo) directory)))
  (external-program:run *cvs-command*
			`("-d" ,(repository repo)
			  "checkout" "-d" ,(namestring target-dir)
			  ,(module repo)))
  target-dir)


(defclass svn-repo (repo)
  ((type :allocation :class :initform "Subversion" :initarg :%type :reader %type)
   (source :initarg :source :reader source))
  (:documentation "Repo managed by Subversion version control system."))

(defmethod vc-get ((repo svn-repo) directory
		   &aux (target-dir (merge-directory-pathname (repo-name-as-string repo) directory)))
  (external-program:run *svn-command* `("checkout" ,(source repo) ,(namestring target-dir)))
  target-dir)


(defclass git-repo (repo)
  ((type :allocation :class :initform "Git" :initarg :%type :reader %type)
   (source :initarg :source :reader source))
  (:documentation "Repo managed by Git version control system."))

(defmethod vc-get ((repo git-repo) directory
		   &aux (target-dir (merge-directory-pathname (repo-name-as-string repo) directory)))
  (external-program:run *git-command* `("clone" ,(source repo) ,(string-right-trim '(#\/) (namestring target-dir))))
  target-dir)

(defclass hg-repo (repo)
  ((type :allocation :class :initform "Mercurial" :initarg :%type :reader %type)
   (source :initarg :source :reader source))
  (:documentation "Repo managed by Mercurial version control system."))

(defmethod vc-get ((repo hg-repo) directory
                   &aux (target-dir (merge-directory-pathname (repo-name-as-string repo) directory)))
  (external-program:run *hg-command* `("clone" ,(source repo)
                                               ,(string-right-trim '(#\/) (namestring target-dir))))
  target-dir)

;; TODO: generalize for e.g. ZIP files
(defclass tarball-repo (repo)
  ((type :allocation :class :initform "Tarball" :initarg :%type :reader %type)
   (source :initarg :source :reader source))
  (:default-initargs :system-pathnames (list (make-pathname :directory '(:relative :wild) :name :wild :type "asd")))
  (:documentation "Pseudo-repo that downloads package from tarball."))

(defun string-ends-with (str ending)
  (string-equal (subseq str (- (length str) (length ending)))
                ending))

(defmethod vc-get ((repo tarball-repo) directory
                   &aux (target-dir (merge-directory-pathname (repo-name-as-string repo) directory)))
  (ensure-directories-exist target-dir)
  (let* ((tarball (get-file (source repo) target-dir)))
    (external-program:run *tar-command*
			  `("-C" ,(namestring target-dir)
                                 ,@(cond
                                    ((string-ends-with (namestring tarball) ".tar") nil)
                                    ((string-ends-with (namestring tarball) ".tar.gz") '("-z"))
                                    ((string-ends-with (namestring tarball) ".tgz") '("-z"))
                                    ((string-ends-with (namestring tarball) ".tar.bz2") '("-j")))
                                 "-xvf",(#-openmcl namestring #+openmcl ccl::native-translated-namestring
                                                   tarball)))) ; FIXME: namestring, .tar.bz2, ...
  target-dir)

;;; Define libraries in terms of repositories

(defun %deflibrary (new-repo)
  (or (find-if #'(lambda (repo)
                   (repo-equalp repo new-repo))
               *libraries*)
      (car (push new-repo *libraries*))))

(defmacro deflibrary (name class &rest args)
  "Define new repo or return an existing one."
  `(%deflibrary (make-instance ',class :name ',name ,@args)))

(defun get-library-repositories (name &aux (name-as-keyword (canonicalize-library-name name)))
  "Get repositories for library named NAME."
  (remove-if-not #'(lambda (repo)
                     (eq (name repo) name-as-keyword))
                 *libraries*))

(defun find-library (name &aux (libraries (get-library-repositories name)))
  "Get a single library named NAME."
  (cond
    ((null libraries)
     (when (and *asdf-install-fallback*
                (or (not (eq :ask *asdf-install-fallback*))
                    (yes-or-no-p "Don't know how to find library ~A.  Fall back to ASDF-Install?" name)))
       (%deflibrary (make-instance 'tarball-repo
                                   :name name
                                   :source (concatenate 'string "asdf-install:" (string name))))))
    
    ((null (rest libraries))
     (first libraries))
    (t (nth (1-
             (loop
                with n-libraries = (length libraries)
                for response = (progn
                                 (format t "More than one repo named ~A found:~%~{  ~D. ~A~%~}--> "
                                         (canonicalize-library-name name)
                                         (loop
                                            for lib in libraries
                                            for i from 1
                                            collect i
                                            collect lib))
                                 (force-output)
                                 (read))
                when (and (numberp response)
                          (<= 1 response n-libraries))
                return response))
            libraries))))

(defun undeflibrary (repo)
  "Remove REPO (name or repo object) from list of known libraries."
  
  (let ((libraries (if (subtypep (type-of repo)
                                 (find-class 'repo))
                       (list repo)
                       (get-library-repositories repo))))
    (setf *libraries*
          (delete-if #'(lambda (old-repo)
                         (member old-repo libraries :test #'repo-equalp))
                     *libraries*))))
