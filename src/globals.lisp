;;;; -*- lisp -*-
;;;; Copyright (c) 2007-2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.
;;;; See COPYING file for details.
(in-package #:cl-librarian)

(defvar *librarian-system-dir*
  (make-pathname :name nil :type nil :version nil
                 :defaults (asdf:component-pathname
                            (asdf:find-system :cl-librarian)))
  "Top CL-Librarian directory")

(defvar *shelves-dir*
  (merge-pathnames "shelves/" *librarian-system-dir*)
  "Directory containing library shelves.")

(defvar *shelved-systems-dir*
  (merge-pathnames "systems/" *librarian-system-dir*)
  "Directory containing .asd files for libraries managed by Librarian.")

(pushnew *shelved-systems-dir* asdf:*central-registry*)

(defvar *git-command* "git")
(defvar *hg-command* "hg")
(defvar *svn-command* "svn")
(defvar *cvs-command* "cvs")
(defvar *darcs-command* "darcs")
(defvar *wget-command* "wget")
(defvar *scp-command* "scp")
(defvar *tar-command* "tar")
(defvar *patch-command* "patch")

(defparameter +cclan-mirrors+
  '("http://ftp.linux.org.uk/pub/lisp/cclan/" ; Leeds UK
    "ftp://ftp.ntnu.no/pub/lisp/cclan/"       ; Uninett, Norway
    "http://thingamy.com/cclan/"              ; (Atsat), Nice, France
    "http://www-jcsu.jesus.cam.ac.uk/ftp/pub/cclan/")) ; Janet, Cambridge UK

(defvar *cclan-mirror* "http://ftp.linux.org.uk/pub/lisp/cclan/")

(defvar *asdf-install-fallback* :ask
  "Defines, what Librarian should do when asked for nonexistent library.
NIL - throws an error
T - falls back automatically to ASDF-Install source
:ASK - asks user before falling back to ASDF.")
