;;;; -*- lisp -*-
;;;; Copyright (c) 2007-2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.
;;;; See COPYING file for details.
(defpackage #:cl-librarian.system
  (:use :common-lisp :asdf))
(in-package #:cl-librarian.system)

(defsystem #:cl-librarian
  :name "CL Librarian"
  :maintainer "Maciek Pasternacki <maciej@pasternacki.net>"
  :author "Maciek Pasternacki <maciej@pasternacki.net>"
  :licence "BSD sans advertising clause (see file COPYING for details)"
  :description "Library packs manager for Common Lisp"

  :depends-on (#:external-program #:split-sequence)
  :components
  ((:module #:src
            :components
            ((:file "package")
             (:file "globals" :depends-on ("package"))
             (:file "utils" :depends-on ("package"))
             (:file "downloading" :depends-on ("package" "globals" "utils"))
             (:file "repo" :depends-on ("package" "utils" "globals" "downloading"))
             (:file "shelves" :depends-on ("package" "globals" "utils" "downloading" "repo"))))))
