#!/bin/sh
#
# bootstrap.sh -- download external dependencies for Common Lisp Librarian
#
# Copyright (c) 2007-2008, Maciej Pasternacki <maciej@pasternacki.net>
# All rights reserved.
# See COPYING file for details.

set -e

if [ ! -f ./cl-librarian.asd ]; then
    echo "$0: please run this script from CL-Librarian directory." >&2
    exit 1
fi

if [ -d ./external ]; then
    echo "$0: ./external directory already exists." >&2
    exit 1;
fi

BASEDIR=`pwd`
mkdir external
cd external

wget http://common-lisp.net/project/external-program/releases/external-program_latest.tar.gz
tar -xzvf external-program_latest.tar.gz

wget http://ftp.linux.org.uk/pub/lisp/cclan/split-sequence.tar.gz
tar -xzvf split-sequence.tar.gz

cd $BASEDIR
ln -sv ./external/*/*.asd .
